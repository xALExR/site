"""Add confirmed and email column for users table

Revision ID: ae0e11bb9594
Revises: 885bd06483d9
Create Date: 2017-02-20 17:19:13.331201

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ae0e11bb9594'
down_revision = '885bd06483d9'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('user', sa.Column('email', sa.VARCHAR(255)))
    op.add_column('user', sa.Column("confirmed", sa.BOOLEAN, nullable=False, default=False))


def downgrade():
    with op.batch_alter_table("user") as batch_op:
        batch_op.drop_column('email')
        batch_op.drop_column('confirmed')
