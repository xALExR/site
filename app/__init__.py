from .view import app
from .model import db
from .utils import mail

db.init_app(app=app)
mail.init_app(app=app)
