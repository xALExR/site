from wtforms import Form, validators, PasswordField, StringField
from wtforms.fields.html5 import EmailField


class LoginForm(Form):
    login = StringField('Login', validators=[validators.Length(min=3, max=255)])
    password = PasswordField('Password', validators=[validators.DataRequired()])


class RegistrationForm(LoginForm):
    confirm_password = PasswordField('Confirm Password', validators=[
        validators.DataRequired(),
        validators.EqualTo('password', message='Passwords must match')
    ])
    email = EmailField('Email', validators=[validators.Length(min=10, max=255), validators.Email('Email must match')])

