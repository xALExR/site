from flask import url_for
from .utils import generate_confirmation_token
from init import app, db


class User(db.Model):
    __table_name__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.VARCHAR(32))
    password = db.Column(db.VARCHAR(255))
    email = db.Column(db.VARCHAR(255))
    confirmed = db.Column(db.Boolean, nullable=False, default=False)
    obfuscated_name = db.Column(db.CHAR(32), default=app.config.get('DEFAULT_AVATAR'))

    def __init__(self, login, email, password, confirmed=False):
        self.login = login
        self.password = password
        self.email = email
        self.confirmed = confirmed
        self.obfuscated_name = app.config.get('DEFAULT_AVATAR')

    def __repr__(self):
        return str("User: login-{login}, password-{password}, email-{email}, confirmed-{confirmed}"
                   .format(login=self.login, password=self.password, email=self.email, confirmed=self.confirmed))

    def link2avatar(self) -> str:
        return url_for('image', filename=self.obfuscated_name)


class ControlInterface:
    def __init__(self, table: db.Model):
        self._table = table

    def __getattr__(self, item):
        return getattr(self._table, item)

    def get_all_users(self):
        try:
            users = self._table.query.all()
        except Exception as e:
            return e.args
        return users

    def login_user(self, login: str, password: str):
        user = self._table.query.filter_by(login=login, password=password).first()
        if user is not None:
            result = {'login': user.login, 'email': user.email, 'confirmed': user.confirmed}
            return result
        return user

    def get_user(self, login: str, email: str):
        user = self._table.query.filter_by(login=login, email=email).first()
        return user

    def registration_user(self, login: str, email: str, password: str):
        check_user = self._table.query.filter_by(login=login).first()
        if check_user is None:
            user = self._table(login=login, email=email, password=password, )
            try:
                db.session.add(user)
                db.session.commit()
            except Exception as e:
                return e.args
            token = generate_confirmation_token(email=email, login=login)
            return token
        else:
            return None

    def confirm(self, user):
        try:
            db.session.add(user)
            db.session.commit()
            return True
        except Exception:
            db.session.rollback()
            return False

    def add_image_to_user(self, user: dict, obs_filename: str):
        import os
        current_user = self.get_user(login=user['login'], email=user['email'])
        if current_user.obfuscated_name not in app.config['DEFAULT_AVATAR']:
            remove_file = os.path.join(app.config['UPLOAD_DIR'], ''.join(current_user.obfuscated_name))
            os.remove(remove_file)
        current_user.obfuscated_name = obs_filename
        try:
            db.session.add(current_user)
            db.session.commit()
        except Exception:
            db.session.rollback()
            raise Exception('Error adding to database', 'error')







