#!/home/xalex/PycharmProjects/env/bin/python3.5
# import socket
import select, socket, sys
from queue import Queue


# def singleton(cls):
#     instances = {}
#     def getinstance(*args, **kwargs):
#         if cls not in instances:
#             instances[cls] = cls(*args, **kwargs)
#         return instances[cls]
#     return getinstance


class Singleton:
    def __new__(cls):
        if not hasattr(cls, '_instance'):
            cls._instance = super(Singleton, cls).__new__(cls)
        return cls._instance


# @singleton
class server(Singleton):
    def bind_server(self, address, port):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((address, port))
        print("Socket run on {}".format(port))
        self.socket.listen(5)

    def run_forever(self):
        while True:
            conn, data = self.socket.accept()
            msg = conn.recv(1024)
            conn.send(msg)
            conn.close()

if __name__ == '__main__':
    pass
    # try:
    #     s = server()
    #     s.bind_server('127.0.0.1', 31456)
    #     s.run_forever()
    # except Exception as e:
    #     print(e.args)

    # server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # server.setblocking(0)
    # server.bind(('localhost', 50000))
    # server.listen(5)
    # inputs = [server]
    # outputs = []
    # message_queues = {}
    #
    # while inputs:
    #     readable, writable, exceptional = select.select(
    #         inputs, outputs, inputs)
    #     for s in readable:
    #         if s is server:
    #             connection, client_address = s.accept()
    #             connection.setblocking(0)
    #             inputs.append(connection)
    #             message_queues[connection] = Queue()
    #         else:
    #             data = s.recv(1024)
    #             if data:
    #                 message_queues[s].put(data)
    #                 if s not in outputs:
    #                     outputs.append(s)
    #             else:
    #                 if s in outputs:
    #                     outputs.remove(s)
    #                 inputs.remove(s)
    #                 s.close()
    #                 del message_queues[s]
    #
    #     for s in writable:
    #         try:
    #             next_msg = message_queues[s].get_nowait()
    #         except Queue.empty:
    #             outputs.remove(s)
    #         else:
    #             s.send(next_msg)
    #
    #     for s in exceptional:
    #         inputs.remove(s)
    #         if s in outputs:
    #             outputs.remove(s)
    #         s.close()
    #         del message_queues[s]
    #
