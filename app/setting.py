import os

SECURITY_PASSWORD_SALT = 'tre34w&$e5r6t7yuipoki89372gre3qwe2@34'
SECRET_KEY = "FDFTYtrf2y3tugey8236efdw7yb32ydqwqdwe"
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
UPLOAD_DIR = os.path.join(BASE_DIR, 'uploads')
ALLOWED_UPLOAD_FILE_EXT = ['.png', '.jpg', '.jpeg']
DEFAULT_AVATAR = 'default-avatar.png'

#  configuration for mysql
DB_TYPE = "mysql"
DB_NAME = "auth"
USER_NAME = "root"
USER_PASS = "root"
HOST_NAME = "127.0.0.1"
SQLALCHEMY_DATABASE_URI = '{mysql}+pymysql://{user_name}:{user_pass}@{host_name}/{database}'\
    .format(mysql=DB_TYPE, user_name=USER_NAME, user_pass=USER_PASS, host_name=HOST_NAME, database=DB_NAME)
SQLALCHEMY_ECHO = True
SQLALCHEMY_TRACK_MODIFICATIONS = True

# config for Flask-mail
MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 465
MAIL_USE_SSL = True
MAIL_USERNAME = 'example@gmail.com'
MAIL_PASSWORD = 'example'

MAIL_SERVER_DIR = os.path.join(BASE_DIR, 'app')
