import os
from datetime import datetime
from threading import Thread

from flask import current_app
from flask_mail import Message
from itsdangerous import URLSafeTimedSerializer

from init import mail


# def run_coroutine(coroutine):
#     try:
#         coroutine.send(None)
#     except StopIteration as e:
#         return e.value


def send_mail_async(app, message):
    with app.app_context():
        mail.send(message)


def send_mail(email, subject, html):
    app = current_app._get_current_object()
    message = Message(
        subject=subject,
        recipients=[email],
        sender=current_app.config['MAIL_USERNAME']  # MAIL_USERNAME
    )
    message.html = html
    message.body = html
    # send(app=app, message=message)
    thr = Thread(target=send_mail_async, args=(app, message))
    thr.start()


def generate_confirmation_token(email: str, login: str) -> str:
    serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    return serializer.dumps(email+'|'+login, salt=current_app.config['SECURITY_PASSWORD_SALT'])


def confirm_token(token, expiration=3600):
    serializer = URLSafeTimedSerializer(current_app.config['SECRET_KEY'])
    try:
        email = serializer.loads(
            token,
            salt=current_app.config['SECURITY_PASSWORD_SALT'],
            max_age=expiration
        )
    except Exception:
        return False
    return email.split('|')


def create_obs_filename(name: str):
    import hashlib
    body, ext = os.path.splitext(name)
    md5 = hashlib.md5()
    md5.update(str('%s.%s' % (body, datetime.now().timestamp())).encode())
    return md5.hexdigest(), ext.lower()


def run_mail_server(app):
    from subprocess import Popen
    import stat, os
    with app.app_context():
        server_name = os.path.join(app.config['MAIL_SERVER_DIR'], 'server.py')

        if not os.access(server_name, os.X_OK):
            os.chmod(server_name, stat.S_IRWXU)
        Popen(server_name, shell=False)




