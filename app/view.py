import os
from flask import request, render_template, url_for, redirect, session, flash, send_from_directory
from functools import wraps, partial

from .model import User, ControlInterface
from .forms import LoginForm, RegistrationForm
from .utils import send_mail, confirm_token, create_obs_filename
from init import app

app.config.from_object('app.setting')

intef_for_user_table = ControlInterface(table=User)


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        try:
            try:
                session['user']
            except KeyError:
                return redirect(url_for('login_user'))
            if not session['user']['confirmed']:
                if str(request.url_rule) != '/logout':
                    raise KeyError()
        except KeyError:
            return "Please confirm registration"
        return f(*args, **kwargs)
    return decorated


@app.route('/')
@app.route('/index/')
def index():
    users = intef_for_user_table.get_all_users()
    return render_template('index.html', users=users)


@app.route('/login', methods=['GET', 'POST'])
def login_user():
    login_form = LoginForm(request.form)
    if request.method == "POST" and login_form.validate():
        log = login_form.login.data  # request.form['login']
        password = login_form.password.data  # request.form['password']
        log_user = intef_for_user_table.login_user(login=log, password=password)
        if log_user is not None:
            session['user'] = log_user
            return redirect(url_for('index'))
        else:
            return "Wrong user"
    return render_template('login.html', form=login_form)


@app.route('/logout')
@requires_auth
def logout():
    if "user" in session:
        session.pop('user')
    return redirect(request.args.get('next') or url_for('index'))


@app.route('/registration', methods=['GET', 'POST'])
def get_registration_form():
    registration_form = RegistrationForm(request.form)
    if request.method == "POST" and registration_form.validate():
        user_login = registration_form.login.data  # request.form['login']
        email = registration_form.email.data  # request.form['email']
        password = registration_form.password.data  # request.form['password']
        token = intef_for_user_table.registration_user(login=user_login, email=email, password=password)
        if token is None:
            return "Incorrect data user with this email or login exists"
        url_confirm = url_for('confirm_email', token=token, _external=True)
        html = render_template('activate.html', confirm_url=url_confirm)
        subject = "Please confirm your email"
        send_mail(email=email, subject=subject, html=html)
        flash('A confirmation email has been sent via email.', 'success')
        return redirect(url_for("login_user"))
    return render_template('registration_form.html', form=registration_form)


@app.route("/confirm/<token>")
def confirm_email(token):
    try:
        email, login = confirm_token(token=token)
    except Exception:
        flash('The confirmation link is invalid or has expired.', 'danger')
    user = intef_for_user_table.query.filter_by(email=email, login=login).first_or_404()
    if user.confirmed:
        flash('Account already confirmed.', 'success')
        return redirect(url_for('login_user'))
    else:
        user.confirmed = 1
        send_to_confirm = intef_for_user_table.confirm(user=user)
        if send_to_confirm:
            flash('You have confirmed your account. Thanks!', 'success')
            return redirect(url_for('index'))
        else:
            flash('Error', 'danger')
        return redirect(url_for('index'))


@app.route('/profile')
@requires_auth
def profile():
    return render_template('profile.html', avatar=User.query.filter_by(
        email=session['user']['email'],
        login=session['user']['login']
    ).first())


@app.route('/get/<filename>', methods=["GET"])
def image(filename):
    return send_from_directory(app.config['UPLOAD_DIR'], filename=filename)


@app.route('/upload', methods=['POST', 'GET'])
def upload():
    if 'file_to_upload' in request.files:
        file = request.files['file_to_upload']
        if file.filename == '':
            flash('No selected file')
        else:
            try:
                obs_filename = create_obs_filename(file.filename)
                if obs_filename[1] not in app.config['ALLOWED_UPLOAD_FILE_EXT']:
                    raise Exception('Not valid type', 'error')
                dst_file = os.path.join(app.config['UPLOAD_DIR'], ''.join(obs_filename))
                file.save(dst_file)
                intef_for_user_table.add_image_to_user(session['user'], ''.join(obs_filename))
            except Exception as e:
                flash(''.join(e.args[0]), e.args[1])
    return redirect(url_for('profile'))


@app.route('/private', methods=["GET"])
@requires_auth
def private():
    return "private page"




