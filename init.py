from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail

app = Flask('view')
db = SQLAlchemy()
mail = Mail()


from app.view import *
