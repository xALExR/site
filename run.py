from init import app
from app.utils import run_mail_server


if __name__ == '__main__':
    run_mail_server(app=app)
    app.run(debug=True)


